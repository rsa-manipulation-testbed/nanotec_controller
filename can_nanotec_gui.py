#!/usr/bin/env python 3
from tkinter import *
import canopen
from canopen.profiles.p402 import BaseNode402


class MainGUI:
    def __init__(self, node, width=300, height=75):
        # Create a window
        window = Tk()

        # Set a title
        window.title("Motor speed")

        # nanotec node
        self.node = node

        self.canvas = Canvas(window, bg="white", width=width, height=height)
        self.canvas.pack()

        # Bind canvas with key events
        self.canvas.bind("<Left>", self.left)
        self.canvas.bind("<Right>", self.right)
        self.canvas.focus_set()

        # speed indicator lines
        self.x = width / 2
        self.y = height / 2

        labeled_ticks = [-120, -90, -60, -30, 0, 30, 60, 90, 120]
        for line in labeled_ticks:
            self.canvas.create_line(self.x + line, self.y + 10, self.x + line, self.y - 10, tags="line")
            self.canvas.create_text(self.x + line, self.y + 15, fill="darkblue", font="Times 7 bold",
                                    text=str(round(line / 60, 2)))

        small_ticks = [-105, -75, -45, -15, 15, 45, 75, 105]
        for line in small_ticks:
            self.canvas.create_line(self.x + line, self.y + 10, self.x + line, self.y - 10, tags="line")

        # Starting tick to indicate current speed
        self.speed_line = self.canvas.create_line(self.x, self.y + 12, self.x, self.y - 12,
                                                  tags="line", width=4, fill="red")

        # Motor starting speed
        self.speed = 0

        # Bind quit button to terminate gui + processes
        Button(window, text="Quit", command=window.destroy).pack()

        # Create an event loop
        window.mainloop()

    def left(self, event):
        self.speed -= 15
        self.node.sdo['vl target velocity'].raw = self.speed
        self._update_speed_indicator()

    def right(self, event):
        self.speed += 15
        self.node.sdo['vl target velocity'].raw = self.speed
        self._update_speed_indicator()

    def _update_speed_indicator(self):
        self.canvas.delete(self.speed_line)
        self.speed_line = self.canvas.create_line(self.x+self.speed, self.y + 12, self.x+self.speed, self.y - 12,
                                                  tags="line", width=4, fill="red")


def setup_network():
    # Create Network
    network = canopen.Network()

    # Create and add Nanotec controller as a node with the 402 profile
    node_nano = BaseNode402(1, 'C5-E-1-09.eds')
    network.add_node(node_nano)

    network.connect(channel="can0", bustype='socketcan', bitrate=1000000)

    node_nano.nmt.state = 'RESET COMMUNICATION'
    # node.nmt.state = 'RESET'
    node_nano.nmt.wait_for_bootup(15)

    # Set controller and motor parameters
    node_nano.sdo['Motor drive submode select'].raw = 0  # Open loop control
    node_nano.sdo['Modes of operation'].raw = 0x02  # Velocity mode
    node_nano.sdo['Polarity'].raw = 0x01  # Clockwise
    node_nano.sdo['Max motor current'].raw = 2000  # Peak current (mA), can be set up to 4200 for our stepper
    node_nano.sdo['Pole pair count'].raw = 50  # This should already be the default

    # Read, clear, and the bind the controlword and target velocity rpdos, and then save
    node_nano.rpdo.read()
    node_nano.rpdo[1].clear()
    node_nano.rpdo[1].add_variable('Controlword')
    node_nano.rpdo[1].add_variable('vl target velocity')
    node_nano.rpdo.save()

    node_nano.setup_402_state_machine()  # Initialize the 402 state machine
    node_nano.sdo['Controlword'].raw = 0x70  # Send command to controlword to reset any errors
    node_nano.sdo['Controlword'].raw = 0x80  # Halt the device and switch it on
    node_nano.sdo['Controlword'].raw = 0x81  # Keep the device halted and enable operation

    # Get device to an operational state - must step through each stage (supposedly)
    node_nano.state = 'READY TO SWITCH ON'
    node_nano.state = 'SWITCHED ON'
    node_nano.state = 'OPERATION ENABLED'

    return node_nano, network


if __name__ == "__main__":
    node, network = setup_network()

    # Run GUI
    MainGUI(node)

    # Shutoff device
    node.state = 'SWITCHED ON'
    node.state = 'READY TO SWITCH ON'
    node.state = 'SWITCH ON DISABLED'
    node.nmt.state = 'PRE-OPERATIONAL'

    # Disconnect network
    network.disconnect()
