#!/usr/bin/env python 3
import canopen
from canopen.profiles.p402 import BaseNode402
import time
import os


def keyboard_control(node, vel=60):
    """
    :param node: Node to control
    :param vel: rotation speed in rpm/60 (functionally rotations per second)
    :return: None
    """
    key_press = None
    while key_press != 'q':
        key_press = input('Direction: ')
        if key_press == 'd':
            node.sdo['vl target velocity'].raw = vel
        elif key_press == 'a':
            node.sdo['vl target velocity'].raw = -vel
        elif key_press == 's':
            node.sdo['vl target velocity'].raw = 0
        else:
            pass


if __name__ == "__main__":
    # NOT WORKING - Try to initialize CAN bus
    # In the mean time, have to run the commands in the terminal
    # os.system('sudo modprobe peak_usb')
    # os.system('sudo ip link set can0 up type can bitrate 1000000')
    # time.sleep(0.1)

    # Create Network
    network = canopen.Network()

    # Create and add Nanotec controller as a node with the 402 profile
    node_nano = BaseNode402(1, 'C5-E-1-09.eds')
    network.add_node(node_nano)

    network.connect(channel="can0", bustype='socketcan', bitrate=1000000)

    node_nano.nmt.state = 'RESET COMMUNICATION'
    # node.nmt.state = 'RESET'
    node_nano.nmt.wait_for_bootup(15)

    # Set controller and motor parameters
    node_nano.sdo['Motor drive submode select'].raw = 0  # Open loop control
    node_nano.sdo['Modes of operation'].raw = 0x02  # Velocity mode
    node_nano.sdo['Polarity'].raw = 0x01  # Clockwise
    node_nano.sdo['Max motor current'].raw = 2000  # Peak current (mA), can be set up to 4200 for our stepper
    node_nano.sdo['Pole pair count'].raw = 50  # This should already be the default

    # Read, clear, and the bind the controlword and target velocity rpdos, and then save
    node_nano.rpdo.read()
    node_nano.rpdo[1].clear()
    node_nano.rpdo[1].add_variable('Controlword')
    node_nano.rpdo[1].add_variable('vl target velocity')
    node_nano.rpdo.save()


    node_nano.setup_402_state_machine()  # Initialize the 402 state machine
    node_nano.sdo['Controlword'].raw = 0x70  # Send command to controlword to reset any errors
    node_nano.sdo['Controlword'].raw = 0x80  # Halt the device and switch it on
    node_nano.sdo['Controlword'].raw = 0x81  # Keep the device halted and enable operation

    # Get device to an operational state - must step through each stage (supposedly)
    node_nano.state = 'READY TO SWITCH ON'
    node_nano.state = 'SWITCHED ON'
    node_nano.state = 'OPERATION ENABLED'

    # Run a basic little teleop
    # a = ccw, d = cw, s = stop, q = quit
    keyboard_control(node_nano)

    # Cycle back to a standby setting - Don't know if this is necessary
    node_nano.state = 'SWITCHED ON'
    node_nano.state = 'READY TO SWITCH ON'
    node_nano.state = 'SWITCH ON DISABLED'
    node_nano.nmt.state = 'PRE-OPERATIONAL'

    network.disconnect()