#!/usr/bin/env python 3
import canopen
from canopen.profiles.p402 import BaseNode402
import time
import os


def deg_to_input(deg):
    # This function works for the factory default unit settings for the nanotec controller
    # For a 1.8 deg step stepper the default input corresponds to a 1/10 step or 1.8/10
    # To complete one revolution this input would be 2000
    return deg / 0.18


if __name__ == "__main__":
    # NOT WORKING - Try to initialize CAN bus
    # In the mean time, have to run the commands in the terminal
    # os.system('sudo modprobe peak_usb')
    # os.system('sudo ip link set can0 up type can bitrate 1000000')
    # time.sleep(0.1)

    # Create Network
    network = canopen.Network()

    # Create and add Nanotec controller as a node with the 402 profile
    node_nano = BaseNode402(1, 'C5-E-1-09.eds')
    network.add_node(node_nano)

    network.connect(channel="can0", bustype='socketcan', bitrate=1000000)

    node_nano.nmt.state = 'RESET COMMUNICATION'
    node_nano.nmt.wait_for_bootup(15)

    # Set controller and motor parameters
    node_nano.sdo['Motor drive submode select'].raw = 0  # Open loop control
    node_nano.sdo['Modes of operation'].raw = 1  # profile position mode
    node_nano.sdo['Polarity'].raw = 0x01  # Clockwise
    node_nano.sdo['Max motor current'].raw = 2000  # Peak current (mA), can be set up to 4200 for our stepper
    node_nano.sdo['Pole pair count'].raw = 50  # This should already be the default

    node_nano.sdo['Profile acceleration'].raw = 100
    node_nano.sdo['Profile deceleration'].raw = 100
    node_nano.sdo['Profile velocity'].raw = 150
    node_nano.sdo[0x607B][1].raw = -2000
    node_nano.sdo[0x607B][2].raw = 2000
    node_nano.sdo[0x607D][1].raw = -2000
    node_nano.sdo[0x607D][2].raw = 2000

    node_nano.sdo['Positioning option code'].raw = 0x01

    # Read, clear, and the bind the controlword and target velocity rpdos, and then save
    node_nano.rpdo.read()
    node_nano.rpdo[1].clear()
    node_nano.rpdo[1].clear()
    node_nano.rpdo[1].add_variable('Controlword')
    node_nano.rpdo[1].add_variable('Target Position')
    node_nano.rpdo.save()

    # # Bind TPDOs
    node_nano.tpdo.read()
    node_nano.tpdo[1].clear()
    node_nano.tpdo[1].add_variable('Statusword')
    node_nano.tpdo[1].add_variable('Position actual internal value')
    node_nano.tpdo.save()

    # Initialize the 402 state machine
    node_nano.setup_402_state_machine()
    print(node_nano.state)
    node_nano.sdo['Controlword'].raw = 0x70  # Send command to controlword to reset any errors

    # Transition states from "SWITCH ON DISABLED" to "OPERATION ENABLED"
    # Ready to switch on
    node_nano.rpdo[1]['Controlword'].raw = 0x06
    node_nano.rpdo[1].transmit()
    time.sleep(0.1)
    print(node_nano.state)

    # Switch on
    node_nano.rpdo[1]['Controlword'].raw = 0x07
    node_nano.rpdo[1].transmit()
    time.sleep(0.1)
    print(node_nano.state)

    # # Enable operation
    node_nano.rpdo[1]['Controlword'].raw = 0x0F
    node_nano.rpdo[1].transmit()
    time.sleep(0.1)
    print(node_nano.state)

    # Try to get motor to move using profile position and the controlword
    node_nano.rpdo[1]['Target Position'].phys = deg_to_input(-200)
    node_nano.rpdo[1].transmit()
    node_nano.rpdo[1]['Controlword'].raw = 0x1F
    node_nano.rpdo[1].transmit()
    time.sleep(0.5)
    print(node_nano.tpdo[1]['Position actual internal value'].raw)
    print(node_nano.sdo[0x6041].raw)

    time.sleep(2)

    # Cycle back to a standby setting
    node_nano.state = 'SWITCH ON DISABLED'
    node_nano.nmt.state = 'PRE-OPERATIONAL'

    network.disconnect()
