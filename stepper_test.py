import canopen
import time
import numpy as np
import matplotlib.pyplot as plt
from canopen.profiles.p402 import BaseNode402


class NanotecController:
    def __init__(self):
        # Setup CanOpen network
        self.network = canopen.Network()

        # Create a node for the nanotec controller and add it to the network
        self.nanotec = BaseNode402(1, 'C5-E-1-09.eds')
        self.network.add_node(self.nanotec)

        # Connect the network
        self.network.connect(channel="can0", bustype="socketcan", bitrate=1000000)

        # Reset device communications
        self.nanotec.nmt.state = 'RESET COMMUNICATION'
        self.nanotec.nmt.wait_for_bootup(15)

        # Apply settings to controller
        self.transmit_controller_settings()

        # Configure PDOs
        self.configure_pdo()

        # Get device into an operational state
        self.ready_device()

        # Other variables
        self.position_buffer = []  # Buffer to store positions when the nanotec internal buffer is full
        self.positions_actual = []  # Actual stepper positions
        self.velocities_actual = []  # Actual stepper velocities
        self.times = []  # Times of recorded positions and velocities
        self.start_time = 0  # Reference time for first movement command

    def transmit_controller_settings(self):
        self.nanotec.sdo['Motor drive submode select'].raw = 0  # Open loop control
        self.nanotec.sdo['Polarity'].raw = 0x01  # Clockwise rotation
        self.nanotec.sdo['Maximum current'].raw = 4200  # Peak current in mA
        self.nanotec.sdo['Motor drive sensor display open loop'][3].raw = -1  # Make vel readings from tick counting
        self.nanotec.sdo['Motor drive sensor display open loop'][4].raw = -1  # Makes pos readings from tick counting

        # Units settings
        self.nanotec.sdo['Velocity numerator'].raw = 1  # Default = 1
        self.nanotec.sdo['Velocity denominator'].raw = 1  # Default = 60, change to 1 to make input RPS
        self.nanotec.sdo['Acceleration numerator'].raw = 1  # Default = 1
        self.nanotec.sdo['Acceleration denominator'].raw = 1  # Default = 60, change to 1 to make input RPS

        # Profile position settings (will want to move some of these so that they can be easily modified for trial
        # sweep)
        self.nanotec.sdo['Modes of operation'].raw = 1  # Profile position mode
        self.nanotec.sdo['Position range limit'][1].raw = -5000  # Min position limit
        self.nanotec.sdo['Position range limit'][2].raw = 5000  # Max position limit
        self.nanotec.sdo['Profile velocity'].raw = 1  # Max speed along trajectory (RPS)
        self.nanotec.sdo['End velocity'].raw = 0  # Velocity at target (RPS)
        self.nanotec.sdo['Profile acceleration'].raw = 0  # Desired starting acceleration (RPS^2)
        self.nanotec.sdo['Profile deceleration'].raw = 0  # Desired braking acceleration (RPS^2)
        self.nanotec.sdo['Motion profile type'].raw = 0  # 0 = unlimited jerk, 3 does other stuff
        self.nanotec.sdo['Max acceleration'].raw = 0.001  # Max acceleration allowed along trajectory
        self.nanotec.sdo['Max deceleration'].raw = 0.001  # Max deceleration allowed along trajectory

    def configure_pdo(self):
        # Configure RPDO's
        self.nanotec.rpdo.read()
        self.nanotec.rpdo[1].clear()
        self.nanotec.rpdo[1].add_variable('Controlword')
        self.nanotec.rpdo[1].add_variable('Target Position')
        self.nanotec.rpdo.save()

        # Configure TPDO's
        self.nanotec.tpdo.read()
        self.nanotec.tpdo[1].clear()
        self.nanotec.tpdo[1].add_variable('Statusword')

        self.nanotec.tpdo[2].clear()
        self.nanotec.tpdo[2].add_variable('Position actual value')
        self.nanotec.tpdo[2].add_variable('Velocity actual value')
        self.nanotec.tpdo.save()

    def ready_device(self):
        # Start state machine
        self.nanotec.setup_402_state_machine()
        print(self.nanotec.state)

        # Ready to switch on
        self.nanotec.rpdo[1]['Controlword'].raw = 0x06
        self.nanotec.rpdo[1].transmit()
        time.sleep(0.1)
        print(self.nanotec.state)

        # Switch on
        self.nanotec.rpdo[1]['Controlword'].raw = 0x07
        self.nanotec.rpdo[1].transmit()
        time.sleep(0.1)
        print(self.nanotec.state)

        # Operation enabled
        self.nanotec.rpdo[1]['Controlword'].raw = 0x0F
        self.nanotec.rpdo[1].transmit()
        time.sleep(0.1)
        print(self.nanotec.state)

    def start_trajectory(self, positions):
        self.position_buffer = positions

        # Set first target position
        self.nanotec.rpdo[1]['Target Position'].phys = self.deg_to_input(self.position_buffer.pop(0))
        self.nanotec.rpdo[1].transmit()

        # Trigger motion via the controlword
        self.nanotec.rpdo[1]['Controlword'].raw = 0x1F  # Triggers motion using absolute position
        self.nanotec.rpdo[1].transmit()

        # Set start time
        self.start_time = time.time()

        # start main trajectory loop
        self.trajectory_main()

    def trajectory_main(self):
        while len(self.position_buffer) > 0:
            # Check bit 13 to see if another position can be buffered
            self.nanotec.tpdo[1].wait_for_reception(0.01)
            status_buffer = self.dec_to_binary(self.nanotec.tpdo[1]['Statusword'].raw)
            print('status buffer bitstring: {}'.format(status_buffer))
            try:
                if status_buffer[-13] == 1:
                    # If bit 12 is set then another setpoint cannot be buffered
                    print('Cannot buffer')
                    pass
            except IndexError:
                self.nanotec.rpdo[1]['Target Position'].phys = self.deg_to_input(self.position_buffer.pop(0))
                self.nanotec.rpdo[1].transmit()

                self.nanotec.rpdo[1]['Controlword'].raw = 0x1F
                self.nanotec.rpdo[1].transmit()
                print('Buffered next setpoint point : {}'.format(status_buffer))

            status_target = 0
            # Wait for current setpoint to be reached and record pos/vel data
            while status_target != '1':
                # Record positions based on internal tick count
                self.nanotec.tpdo[2].wait_for_reception(0.01)
                self.positions_actual.append(self.nanotec.tpdo[2]['Position actual value'].phys)
                self.velocities_actual.append(self.nanotec.tpdo[2]['Velocity actual value'].phys)
                self.times.append(time.time()-self.start_time)

                # Check bit 10 to see if setpoint reached
                try:
                    status_target = self.dec_to_binary(self.nanotec.tpdo[1]['Statusword'].raw)[-11]
                except IndexError:
                    status_target = 0

            # Reset controlword bit 4 so that it can be triggered to start next motion
            self.nanotec.rpdo[1]['Controlword'].raw = 0xF
            self.nanotec.rpdo[1].transmit()

        # Cycle device back to the SWITCH ON DISABLED STATE
        self.nanotec.state = 'SWITCH ON DISABLED'

        # Save time, pos, & vel to a csv
        self.save_csv()

    def save_csv(self):
        # Positions in deg
        positions_array = np.array(list(map(lambda x: x * 0.18, self.positions_actual)), ndmin=2)

        # Velocities in deg/s
        velocities_array = np.array(list(map(lambda x: x * 360, self.velocities_actual)), ndmin=2)
        time_array = np.array(self.times, ndmin=2)

        trial_data = np.concatenate((time_array, positions_array, velocities_array), axis=1)

        # Save trial data to a csv
        np.savetxt('data.csv', trial_data, delimiter=',')

    def plot(self):
        # Positions in deg
        positions_array = list(map(lambda x: x * 0.18, self.positions_actual))

        # Velocities in deg/s
        velocities_array = list(map(lambda x: x * 360, self.velocities_actual))
        time_array = self.times

        plt.plot(time_array, positions_array, time_array, velocities_array)
        plt.legend(['positions', 'velocities'])
        plt.show()

    def disconnect_network(self):
        self.network.disconnect()
        print("DISCONNECTING NETWORK")

    @staticmethod
    def dist_to_input(dist, lead=0.10):
        return 360 * dist / lead / 0.18

    @staticmethod
    def deg_to_input(deg):
        return deg / 0.18

    @staticmethod
    def dec_to_binary(n):
        return bin(n).replace("0b", "")


if __name__ == "__main__":
    stepper = NanotecController()
    # list of angles to go to
    stepper.start_trajectory([360, 0, 360])
    stepper.plot()
    stepper.disconnect_network()
